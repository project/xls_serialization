<?php

namespace Drupal\xls_serialization;

/**
 * Constants of the xls_serialization module.
 */
class XlsSerializationConstants {

  const EXCEL_2007_FORMAT = 'Excel2007';

  const EXCEL_5_FORMAT = 'Excel5';

}
